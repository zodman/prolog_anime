% src basado: https://www.csupomona.edu/~jrfisher/www/prolog_tutorial/2_17.html
% http://alumni.cs.ucr.edu/~vladimir/cs171/prolog_2.pdf
% pag:7.. def retract/cut/assert

sintomas(X,Y):-
    write('tienes '), write(X), write(' '), write(Y), writeln('?'),
    read(Z),
    Z == yes.

tiene(X):-
    enfe(X, Y),
    write('tiene '),
    write(X),write('-'), write(Y).

enfe(influeza,Y):-
    sintomas(estornudos, ocacionales),
    sintomas(dolor_cabeza, fuerte),
    Y = 90.
    
enfe(gripe, Y):-
    sintomas(estornudos, ninguno),
    Y = 10.

go:-
 tiene(influeza);
 tiene(gripe).
