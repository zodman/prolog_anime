

influenza:-
    verify(dolor_cabeza, intenso),
    verify(dolor_cuerpo, fuerte),
    verify(estornudos, frecuentes).

gripe:-
    verify(dolor_cabeza, leve),
    verify(estornudos, frecuentes),
    verify(congestion_nasal, normal).

parainfluenza:-
    verify(dolor_cabeza, fuerte),
    verify(estornudos, normal),
    verify(dolor_cuerpo, fuerte).


ask(S, F):-
    write(' tiene '),
    write(S),
    write(' '),
    write(F),
    write(' ?'),
    nl,
    read(Resp),
     (Resp==y; Resp=yes) -> assert(yes(S,F));
    (assert( no(S,F)), ask_sintoma(S,F), fail).


ask_sintoma(S,X):-
    write(' como siente '),
    write(S),
    write(' '),
    read(O),
    write('tu respuesta:'), write(O),nl,
    assert(new_db(S,O))
    ,true.


verify(Sintoma, Intensidad):-
    yes(Sintoma,Intensidad) -> true; 
    no(Sintoma,Intensidad) -> fail;
    ask(Sintoma,Intensidad).

:- dynamic yes/2,no/2, new_db/2.

puedequesea(influenza):- influenza.
puedequesea(gripe):- gripe.
puedequesea(parainfluenza):- parainfluenza.

borratodo:- retract(yes(_,_)),fail. 
borratodo:- retract(no(_,_)),fail.
borratodo:- retract(new_db(_,_)),fail.
borratodo.

nueva_enf_detectada(Y):-
    findall(X, new_db(X,Z),W),
    findall(X, new_db(Z,X),J),
    write('Nueva enfermedad con: '), write(W), nl,
    write('Sintomas: '), write(J), write('Nombre para la enfermedad:'),
    read(L), Y=L
    .


go:-
    borratodo,
    puedequesea(Enfermedad)->true;nueva_enf_detectada(Enfermedad),
    write('Dectando:'), write(Enfermedad)
    .

