/*
%Tomado de:
%http://art2key.blogspot.mx/2013/03/clasificacion-de-generos-manga-y-anime.html
%Knolege base de los generos de manga y anime japones
*/

thematic(P, CF):-
    P =< CF.


%kodomo
% Este genero es para los niños, la comunidad infantil en general. La temática siempre es de lo más simple para su fácil compresión y actualmente es el género menos publicitado a pesar que en la ultima década han sacada a gran partes de sus exponentes. El punto débil de este género es que por la temática tan simple dejan de lado otras cosas como la música (op/ed) y la trama.  Ejemplos: Doraemon, El dulce Hogar de Chii, Los Gatos Samurai, Hamtaro, Pókemon.

%thematics:
%simple 90%
%funny_parts 20%
%jokes 40%

kodomo_thematic(simple,90).
kodomo_thematic(funny_parts, 20).
kodomo_thematic(jokes, 40).
kodomo_thematic_check([],[]).

kodomo_thematic_check(THEMATICS,PERCENTS):-
    [T|T_] = THEMATICS,
    [P|P_] = PERCENTS,
    kodomo_thematic(T,X),
    _RULE_CF is  P*100/X,
    write(T),write(':'),write(_RULE_CF),writeln('>'),
    kodomo_thematic_check(T_,P_).

kodomo_sex(male).
kodomo_sex(female).
kodomo_target_audencie(AGE,SEX):-(AGE =< 15), kodomo_sex(SEX).
kodomo(AGE, SEX, THEMATICS, PERCENTS):-kodomo_target_audencie(AGE, SEX), kodomo_thematic_check(THEMATICS, PERCENTS). 

[other_ruls].
