
%
% Shojo
% El shojo esta dirigido a niñas o mujeres/adolescentes. Se centras mas en el aspecto romántico amoroso, la amistad y el noviazgo. La protagonista es mujer. Tiende a tener tramas complicadas por conflictos amorosos así como múltiples enredos y partes graciosas.Gran parte de estas series se desarrollan con persones principales de edad escolar para darle mas fluidez a la historia.  Ejemplos: Candy Candy (la madre del género), Karekano, Kaleido Star, Bronze no Tenshi y Ashita No Nadja.

%thematics:
% love 50 -90%
% romantic 30-90%
% funny_parts 40-90%
% school 30-95%




shojo_sex(female).
shojo_thematic(romantic).
shojo_target_audencie(AGE, SEX):- AGE > 15, AGE< 30, shojo_sex(SEX).
shojo(AGE,SEX, THEMATIC):- shojo_target_audencie(AGE,SEX), shojo_thematic(THEMATIC).
is_shojo(AGE,SEX, THEMATIC):- shojo(AGE,SEX,THEMATIC).

/*shonen
*El es genero para niños (hombres).La temática es también un tanto simple pero con mas acción ya sea peleas o una trama mas profunda y algo de romance. Es de los géneros cuyos títulos suelen ser algo mas largos y en el anime cuentan con muchos episodios de relleno.  Ejemplos:  Inuyasha, Naruto, Saint Seiya y Dragon Ball.

thematics:
love: 10-15%
action: 90%
violence: 85%
* */
shonen_sex(male).
shonen_thematic(action).
shonen_thematic(simple).
shonen_target_audencie(AGE,SEX):- shonen_sex(SEX), AGE>10.
shonen(AGE,SEX,THEMATIC):- shonen_target_audencie(AGE,SEX), shonen_thematic(THEMATIC).
is_shonen(AGE,SEX,THEMATIC):- shonen(AGE,SEX,THEMATIC).

