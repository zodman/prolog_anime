% R1
% fiebre media, tento el cuerpo cortado, escalofrios, resfriado comun 60%


% Influenza http://www.vidaysalud.com/diario/vida-saludable/cual-es-la-diferencia-entre-la-gripe-influenza-y-el-resfriado-comun/


influenza_examen_tipoA(SI,CP):-
    SI = true,!,
    writeln('influeza tipo A'),
    CP = 100.

influenza(FIEBRE,DOLOR_CUERPO,DOLOR_CABEZA, ESTORNUDOS,CP):-
    FIEBRE = intensa,
    DOLOR_CUERPO = fuerte,
    DOLOR_CABEZA = fuerte,
    ESTORNUDOS = ocasional,
    writeln('Probablidad de influeza:50%'),
    writeln('Es necesario hacer analisis clinico para influeza '),
    writeln('se encontraron proteinas la hemaglutina (H) y la neuromidasa (N).Responde: true. o false.'),
    read(X),
    influenza_examen_tipoA(X,CP); writeln('influenza'), CP=50.


rinofaringitis(FIEBRE,DOLOR_CUERPO,CABEZA, ESTORNUDOS, CP):-
    FIEBRE = baja,
    DOLOR_CUERPO = leves,
    ( CABEZA = leves; CABEZA = ocasional),
    ESTORNUDOS = ocasional,
    writeln('rinofaringitis/catarro comun'),
    CP = 90.

% http://www.healthychildren.org/Spanish/health-issues/conditions/chest-lungs/Paginas/Parainfluenza-Viral-Infections.aspx
parainfluenza(FIEBRE, DOLOR_CUERPO, DOLOR_CABEZA, ESTORNUDOS,CP):-
    FIEBRE = moderada,
    (DOLOR_CUERPO = leves; DOLOR_CUERPO = false),
    (DOLOR_CABEZA = leves; DOLOR_CABEZA = false),
    ESTORNUDOS = ocasional,
    writeln('parainfluenza'),
    CP = 80.

enfermedad(FIEBRE, DOLOR_CUERPO, DOLOR_CABEZA, ESTORNUDOS,X):-
    fiebre(FIEBRE, DOLOR_CUERPO, DOLOR_CABEZA, ESTORNUDOS, X);
    parainfluenza(FIEBRE, DOLOR_CUERPO, DOLOR_CABEZA, ESTORNUDOS, X);
    influenza(FIEBRE, DOLOR_CUERPO, DOLOR_CABEZA, ESTORNUDOS,X).
